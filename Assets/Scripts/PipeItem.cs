﻿using System.Collections;
using UnityEngine;

public class PipeItem : MonoBehaviour
{
    private Transform rotater;
    private bool isCollided;
    private Vector3 startRotation;
    private Vector3 startRotaterRotation;
    private int direction;
    private float endPoint;
    private Pipe pipe;
    
    private void Awake()
    {
        rotater = transform.GetChild(0);
    }
    private void Start()
    {
        isCollided = false;
        direction = Random.value < 0.5f ? 1 : -1;
    }
    public void Position(Pipe pipe, float curveRotation, float ringRotation)
    {
        this.pipe = pipe;
        
        transform.SetParent(pipe.transform, false);
        transform.localRotation = Quaternion.Euler(0f, 0f, -curveRotation);
        rotater.localPosition = new Vector3(0f, pipe.CurveRadius);
        rotater.localRotation = Quaternion.Euler(ringRotation, 0f, 0f);
    }

    public void ForceToward()
    {
        isCollided = true;
    }

    private void Update()
    {
        if (isCollided)
        {
            float angleStep = pipe.CurveAngle / pipe.CurveSegmentCount;
            endPoint = pipe.CurveSegmentCount * angleStep;
            if (startRotation.z > -endPoint) //In pipe's range
            {
                startRotation.z -= 10f * Time.deltaTime;
                startRotaterRotation.x += 480f * direction * Time.deltaTime;
                transform.localRotation = Quaternion.Euler(startRotation);
                rotater.localRotation = Quaternion.Euler(startRotaterRotation);
            }
            else //Out pipe's range
            {
                gameObject.SetActive(false);
                isCollided = false;
            }
        }
    }
}