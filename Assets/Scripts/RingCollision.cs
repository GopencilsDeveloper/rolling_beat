﻿using UnityEngine;

public class RingCollision : MonoBehaviour
{
	public Ring ring;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
        }
        if (other.tag == "Obstacle")
        {
			ring.gameObject.SetActive(false);
        }
    }
}
