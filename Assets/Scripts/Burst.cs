﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burst : MonoBehaviour
{
    public ParticleSystem burst;
    private void OnTriggerEnter(Collider other)
    {
        burst.Emit(burst.main.maxParticles);
    }
}
