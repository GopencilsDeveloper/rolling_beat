﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollision : MonoBehaviour
{
    public PipeItem pipeItem;
    public ParticleSystem burst;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            // burst.Emit(burst.maxParticles);
            pipeItem.ForceToward();
        }
        if (other.tag == "Ring")
        {
        }
    }
}
