﻿using UnityEngine;

public class EmitBurst : MonoBehaviour
{
    public ParticleSystem burst;

    public void Emit()
    {
        burst.Emit(burst.maxParticles);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
			Emit();
        }
    }
}
