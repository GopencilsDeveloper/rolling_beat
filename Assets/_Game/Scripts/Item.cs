﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item 
{
	[Header("Item")]
	public string itemID;
	public int isUnlocked;
	public int price;
}
