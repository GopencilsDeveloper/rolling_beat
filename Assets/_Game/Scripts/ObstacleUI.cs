﻿using System.Collections;
using UnityEngine;

public class ObstacleUI : MonoBehaviour
{
    Vector2 tempTransform;

    private void Start()
    {
        float tempScale = Random.Range(0.2f, -0.7f);
        tempTransform = gameObject.transform.localPosition;
        transform.SetParent(GameObject.FindGameObjectWithTag("ObstaclesParent").transform);
        transform.localScale = Vector3.one;
        SetPosition(Random.Range(-500, 500), Random.Range(0, -500));
        SetScale(tempScale, tempScale);
    }
    public void SetPosition(float offsetX, float offsetY)
    {
        tempTransform.x += offsetX;
        tempTransform.y += offsetY;
        gameObject.transform.localPosition = tempTransform;
    }
    public void SetScale(float scaleX, float scaleY)
    {
        Vector3 temp = transform.localScale;
        temp.x += scaleX;
        temp.y += scaleY;
        transform.localScale = temp;
    }
}
