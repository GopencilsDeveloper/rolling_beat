﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    private Vector3 originScale;
    private void Start()
    {
        originScale = transform.localScale;
        StartCoroutine(Scale(originScale, new Vector3(100f, 100f, 100f), 120f));
    }
    public IEnumerator Scale(Vector3 fromScale, Vector3 targetScale, float timeToMove)
    {
        var currentScale = transform.localScale;
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / timeToMove;
            transform.localScale = Vector3.Lerp(currentScale, targetScale, t);
            yield return null;
        }
    }
}
