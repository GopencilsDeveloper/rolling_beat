﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour
{
    public Player player;
    private Collider playerCollider;
    private void OnEnable()
    {
        playerCollider = GetComponent<Collider>();
        playerCollider.enabled = false;
        EnableColliderAfterSeconds(3.0f);
    }

    public void EnableColliderAfterSeconds(float second)
    {
        StartCoroutine(C_EnableCollider(second));
    }

    IEnumerator C_EnableCollider(float delay)
    {
        yield return new WaitForSeconds(delay);
        playerCollider.enabled = true;
    }
    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Gem")
        {
            VibrationHandler.Vibrate(20);
            ScoreManager.Instance.AddInGameScore(1);
            SpawnManager.Instance.SpawnStarUI();
            UIManager.Instance.UpdatePercent();
            KoreoManager.Instance.scorePerSong++;
            ScoreManager.Instance.AddScore(1);
            UIManager.Instance.PlayPerfectAnim();
        }
        if (collider.tag == "Wall")
        {
            player.objPlayerCollideWith = collider.transform.parent.parent.parent.parent.gameObject;
            VibrationHandler.Vibrate(50);
            if (UnityAdsManager.Instance.isAdsReady || ScoreManager.Instance.totalScore >= ScoreManager.Instance.reviveGemPrice)
            {
                GameManager.Instance.ReviveGame();
            }
            else
            {
                GameManager.Instance.GameOver();
            }
        }
        if (collider.tag == "MissZone")
        {
            UIManager.Instance.missedScore++;
            UIManager.Instance.PlayMissAnim();
        }
    }
}