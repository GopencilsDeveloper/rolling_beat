﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StarUIController : MonoBehaviour
{
    public void Position()
    {
        transform.SetParent(GameObject.FindGameObjectWithTag("StarsParents").transform, false);
        transform.localPosition = Vector3.zero;
    }

    public void TweenStarToTarget(Transform midPoint, Transform endPoint)
    {
        iTween.MoveTo(gameObject, iTween.Hash(
        "position", endPoint.position,
        "time", 1f,
        "path", new Transform[] { midPoint, endPoint },
        "oncomplete", "OnComplete"));

        gameObject.transform.DOScale(Vector3.zero, 1f);
    }
    void OnComplete()
    {
        transform.localScale = Vector3.one;
        gameObject.SetActive(false);
    }

}
