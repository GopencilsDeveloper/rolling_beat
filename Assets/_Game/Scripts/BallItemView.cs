﻿using UnityEngine;
using UnityEngine.UI;

public class BallItemView : MonoBehaviour
{
    public Text nameBall;
    public Color colorBall;
    public Text priceTxt;
    public Image icon;

    public Button buyBtn;
    public Toggle selectToggle;

    private int price;
    private Color color;

    public void Initialize(string itemID, int price, int isUnlocked, string name, Color color, Sprite icon)
    {
        this.nameBall.text = name;
        this.colorBall = color;
        this.color = color;
        this.priceTxt.text = price.ToString();
        this.price = price;
        this.icon.sprite = icon;
        this.buyBtn.interactable = !(price >= ScoreManager.Instance.TotalScore);
    }

    private void Start()
    {
        buyBtn.onClick.AddListener(OnBuyBtnClicked);
        selectToggle.onValueChanged.AddListener(delegate { OnSelectToggleSelected(selectToggle); });
        selectToggle.gameObject.SetActive(false);
    }
    private void OnBuyBtnClicked()
    {
        ScoreManager.Instance.MinusScore(price);
        buyBtn.gameObject.SetActive(false);
        selectToggle.gameObject.SetActive(true);
    }

    public void OnSelectToggleSelected(Toggle change)
    {
        if (change.isOn)
        {
            UIManager.Instance.playerColor = color;
        }
    }
}
