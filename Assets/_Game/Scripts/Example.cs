﻿using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class Example : MonoBehaviour
{
    AudioSource audio;
    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    [Button]
    void Middle()
    {
        audio.time = audio.clip.length / 2f;
        audio.Play();
    }
}
