﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTest : MonoBehaviour
{
    public float duration = 0.05f;
    public float maxSize = 1.5f;

	private Vector3 originScale;

    // This remains true while it is in the prossess of scaling.
    private bool isScaling = false;

    private void Start()
    {
		originScale = transform.localScale;
    }

    public void DoScale()
    {
        if (gameObject.activeInHierarchy)
        {
            StartCoroutine(C_DoScale());
        }
    }
    private IEnumerator C_DoScale()
    {
        // Check if we are scaling now, if so exit method to avoid overlap.
        if (isScaling)
            yield break;

        // Declare that we are scaling now.
        isScaling = true;

        // Grab the current time and store it in a variable.
        float startTime = Time.time;

        while (Time.time - startTime < duration)
        {
            float amount = (Time.time - startTime) / duration;
            transform.localScale = Vector3.Lerp(originScale, originScale * maxSize, amount);
            yield return null;
        }

        transform.localScale = originScale * maxSize;

        // Leave the scale at maxSize for few seconds (this can be changed at any time).
        // yield return new WaitForSeconds(2.0f);

        // Now for the scale down part.  Store the current time in the same variable.
        startTime = Time.time;

        while (Time.time - startTime < duration)
        {
            float amount = (Time.time - startTime) / duration;
            transform.localScale = Vector3.Lerp(originScale * maxSize, originScale, amount);
            yield return null;
        }

        transform.localScale = originScale;

        // Declare that we are no longer modifing the scale.
        isScaling = false;
    }
}
