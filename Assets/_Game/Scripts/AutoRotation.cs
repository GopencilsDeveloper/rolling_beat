﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotation : MonoBehaviour
{
    public float speedRotation;
    private Vector3 euler;
    private void Start()
    {
        euler = transform.eulerAngles;
    }
    private void Update()
    {
        euler.z -= 5f * speedRotation * Time.deltaTime;
        transform.eulerAngles = euler;
    }
}
