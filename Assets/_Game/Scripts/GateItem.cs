﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateItem : MonoBehaviour
{
    private Transform rotater;
    private void Awake()
    {
        rotater = transform.GetChild(0);
    }
    public void Position(Pipe pipe)
    {
        float angleStep = pipe.CurveAngle / pipe.CurveSegmentCount;
        float curveRotation = (pipe.CurveSegmentCount) * angleStep;

        transform.SetParent(pipe.transform, false);
        transform.localRotation = Quaternion.Euler(0f, 0f, -curveRotation);
        rotater.localPosition = new Vector3(0f, pipe.CurveRadius);
        // rotater.localRotation = Quaternion.Euler(0f, 0f, 0f);
    }
}
