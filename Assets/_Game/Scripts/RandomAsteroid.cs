﻿using UnityEngine;

public class RandomAsteroid : PipeItemGenerator
{
    public Asteroid[] itemPrefabs;

    public override void GenerateItems(Pipe pipe)
    {
        float angleStep = pipe.CurveAngle / pipe.CurveSegmentCount;
        for (int i = 0; i < pipe.CurveSegmentCount / 3f; i++)
        {
            var obj = ObjectPooler.Instance.SpawnFromPool("Asteroid");
            Asteroid asteroid = obj.GetComponent<Asteroid>();
            float pipeRotation =
                (Random.Range(0, pipe.pipeSegmentCount) + 0.5f) *
                360f / pipe.pipeSegmentCount;
            asteroid.Position(pipe, i * angleStep, pipeRotation);
            obj.SetActive(true);
        }
    }
}
