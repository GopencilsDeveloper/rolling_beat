﻿using MoreMountains.NiceVibrations;
using UnityEngine;

public class VibrationHandler : MonoBehaviour
{
    public static void Vibrate(long miliseconds)
    {
#if UNITY_IPHONE
        MMVibrationManager.Haptic (HapticTypes.LightImpact);
#elif UNITY_ANDROID
        AndroidVibration.Vibrate(miliseconds);
#endif
    }
}
