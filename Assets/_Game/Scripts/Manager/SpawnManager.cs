﻿using UnityEngine;

public class SpawnManager : MonoSingleton<SpawnManager>
{
    public Transform[] midPoints;
    public Transform[] endPoints;
    public GameObject gate;

    int count = 0;
    int index = 0;
    int indexColor = 0;

    private void Awake()
    {
        GameManager.GameStateChanged += OnGameStateChanged;
    }
    void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.INGAME:
                index = 0;
                break;
            case GameState.RESTART:
                index = 0;
                break;
            default:
                break;
        }
    }

    public void SpawnNote(Pipe pipe, float curveRotation, bool isSpriral = false, int direction = 1, float multiple = 5)
    {
        count++;
        if (isSpriral)
        {
            if (direction == 1)
            {
                index++;
            }
            else
            {
                index--;
            }
        }
        else
        {
            index += Random.value < 0.5f ? -1 : 1;
        }

        float step = 360f / pipe.pipeSegmentCount;
        float noteRelativeRotation = 0;
        noteRelativeRotation = (index + 0.5f) * step * multiple;

        var obj = ObjectPooler.Instance.SpawnFromPool("Note");
        PipeItem item = obj.GetComponent<PipeItem>();
        item.Position(pipe, curveRotation, noteRelativeRotation);

        item.SetColor();
        obj.SetActive(true);
    }

    public void SpawnGate(Pipe pipe)
    {
        var gateObj = Instantiate(gate);
        GateItem item = gateObj.GetComponent<GateItem>();
        item.Position(pipe);
    }

    public void SpawnAsteroid(Pipe pipe)
    {
        float angleStep = pipe.CurveAngle / pipe.CurveSegmentCount;
        for (int i = 0; i < 40; i++)
        {
            GameObject obj = ObjectPooler.Instance.SpawnFromPool("Asteroid");
            Asteroid item = obj.GetComponent<Asteroid>();
            item.Position(pipe, 3 * Random.Range(0, pipe.CurveSegmentCount / 3f) * angleStep, 0);
            obj.SetActive(true);
        }
    }

    public void SpawnStarUI()
    {
        GameObject obj = ObjectPooler.Instance.SpawnFromPool("StarUI");
        obj.SetActive(true);

        StarUIController starUIobj = obj.GetComponent<StarUIController>();
        starUIobj.Position();

        if (UIManager.Instance.StarsPercentValue >= 0 && UIManager.Instance.StarsPercentValue < 0.33f)
        {
            starUIobj.TweenStarToTarget(midPoints[Random.Range(0, midPoints.Length)], endPoints[0]);
        }
        else
        if (UIManager.Instance.StarsPercentValue >= 0.33f && UIManager.Instance.StarsPercentValue < 0.66f)
        {
            starUIobj.TweenStarToTarget(midPoints[Random.Range(0, midPoints.Length)], endPoints[1]);
        }
        else
        if (UIManager.Instance.StarsPercentValue >= 0.66f && UIManager.Instance.StarsPercentValue <= 1.0f)
        {
            starUIobj.TweenStarToTarget(midPoints[Random.Range(0, midPoints.Length)], endPoints[2]);
        }
    }
}
