﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;

public class ScoreManager : MonoSingleton<ScoreManager>
{
    public const string TOTALSCORE = "TotalScore";
    public int reviveGemPrice = 100;

    [HideInInspector] public int inGameScore;

    [HideInInspector] public int totalScore;

    public int TotalScore
    {
        get
        {
            return totalScore;
        }
    }

    public override void Initialize()
    {
        inGameScore = 0;
        LoadScore();
        GameManager.GameStateChanged += OnGameStateChanged;
    }

    public void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.MAINMENU:
                // AddScore(inGameScore);
                inGameScore = 0;
                LoadScore();
                break;
            case GameState.RESTART:
                inGameScore = 0;
                break;
            case GameState.GAMEOVER:
                // AddScore(inGameScore);
                break;
            case GameState.REVIVE:
                break;
            default:
                break;
        }
    }

    public void AddInGameScore(int score)
    {
        inGameScore += score;
    }
    public void AddScore(int score)
    {
        float temp = totalScore;
        totalScore += score;
        SaveScore();
        UIManager.Instance.UpdateScoreUI();
    }

    public void LoadScore()
    {
        totalScore = PlayerPrefs.GetInt(TOTALSCORE);
    }
    public void MinusScore(int score)
    {
        totalScore -= score;
        SaveScore();
        UIManager.Instance.UpdateScoreUI();
    }
    public void SaveScore()
    {
        PlayerPrefs.SetInt(TOTALSCORE, totalScore);
        PlayerPrefs.Save();
    }

    [Button]
    public void ClearScore()
    {
        MinusScore(totalScore);
    }
    [Button]
    public void Add10000()
    {
        AddScore(10000);
    }
    [Button]
    public void ClearAll()
    {
        PlayerPrefs.DeleteAll();
    }
}
