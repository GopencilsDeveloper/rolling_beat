﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;
using NaughtyAttributes;
using UnityEngine.UI;

[System.Serializable]
public class Song : Item
{
    [Header("Song")]
    public Koreo koreo;
    public string nameSong;
    public int level;
    public float levelEventsPercent;
    public float levelLengthPercent;
    public float CheckPointPercent
    {
        get
        {
            float result = 0;
            if (IsCheckedPoint)
            {
                if (levelLengthPercent < 0.25f)
                {
                    result = 0f;
                }
                else
                if (levelLengthPercent < 0.5f)
                {
                    result = 0.25f;
                }
                else
                if (levelLengthPercent < 0.75f)
                {
                    result = 0.5f;
                }
                else if (levelLengthPercent < 0.99f)
                {
                    result = 0.75f;
                }
            }
            return result;
        }
    }
    public bool IsCheckedPoint
    {
        get
        {
            if (level == 0 && levelLengthPercent < 0.25f)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}

[System.Serializable]
public class Ball : Item
{
    public string name;
    public Color color;
    public Sprite icon;
}
public class ItemsManager : MonoSingleton<ItemsManager>
{
    #region CONST
    public const string IsUNLOCKED = "isUnlocked";
    public const string LEVEL = "level";
    public const string LEVELPERCENT = "levelPercent";
    public const string LEVELLENGTHPERCENT = "levelLengthPercent";
    #endregion

    #region Params
    private Song currentSong;
    private SongView currentSongView;
    #endregion

    #region Properties
    public Song CurrentSong { get { return songs[UIManager.Instance.CurrentSongIndex]; } }
    public SongView CurrentSongView { get { return songViews[UIManager.Instance.CurrentSongIndex]; } }
    #endregion

    #region SONG
    [Header("Song")]
    public GameObject nullSongPrefab;
    public GameObject songPrefab;
    public Transform songListView;
    public List<Song> songs;
    [HideInInspector] public List<SongView> songViews;

    #endregion

    #region BALL
    [Header("Ball")]
    public GameObject ballPrefab;
    public Transform ballListView;
    public List<Ball> ballList;
    #endregion

    #region THEME
    [Header("Theme")]
    public List<Song> themeList;
    #endregion

    public override void Initialize()
    {
        for (int i = 0; i < songs.Count; i++)
        {
            var newSong = Instantiate(songPrefab) as GameObject;
            SongView songView = newSong.GetComponent<SongView>();
            songViews.Add(songView);
            LoadSongInfo(songs[i]);

            songView.Initialize(
                i + 1,
                songs[i],
                songs[i].itemID,
                songs[i].price,
                songs[i].isUnlocked,
                songs[i].nameSong,
                songs[i].level,
                songs[i].levelEventsPercent,
                songs[i].levelLengthPercent);

            newSong.transform.SetParent(songListView, false);
        }

        var nullSong = Instantiate(nullSongPrefab) as GameObject;
        nullSong.transform.SetParent(songListView, false);

        GameManager.GameStateChanged += OnGameStateChanged;
    }

    public void LoadSongInfo(Song song)
    {
        if (song.itemID == "S0")
        {
            song.isUnlocked = 1;
        }
        else
        {
            song.isUnlocked = PlayerPrefs.GetInt(song.itemID + IsUNLOCKED);
        }
        song.level = PlayerPrefs.GetInt(song.itemID + LEVEL);
        song.levelEventsPercent = PlayerPrefs.GetFloat(song.itemID + LEVELPERCENT);
        song.levelLengthPercent = PlayerPrefs.GetFloat(song.itemID + LEVELLENGTHPERCENT);
    }

    public void UpdateSongUI(Song song, SongView songView)
    {
        songView.UpdateSongView(song);
    }

    public void UnlockItem(string itemID)
    {
        PlayerPrefs.SetInt(itemID + IsUNLOCKED, 1);
    }

    public void SetSongInfo(Song song)
    {
        if (song.level > PlayerPrefs.GetInt(song.itemID + LEVEL))
        {
            PlayerPrefs.SetInt(song.itemID + LEVEL, song.level);
            PlayerPrefs.SetFloat(song.itemID + LEVELPERCENT, song.levelEventsPercent);
            PlayerPrefs.SetFloat(song.itemID + LEVELLENGTHPERCENT, song.levelLengthPercent);
        }
        else
        if (song.level == PlayerPrefs.GetInt(song.itemID + LEVEL))
        {
            if (song.levelEventsPercent >= PlayerPrefs.GetFloat(song.itemID + LEVELPERCENT))
            {
                PlayerPrefs.SetFloat(song.itemID + LEVELPERCENT, song.levelEventsPercent);
            }
            if (song.levelLengthPercent >= PlayerPrefs.GetFloat(song.itemID + LEVELLENGTHPERCENT))
            {
                PlayerPrefs.SetFloat(song.itemID + LEVELLENGTHPERCENT, song.levelLengthPercent);
            }
        }
    }

    [Button]
    public void DeleteAll()
    {
        PlayerPrefs.DeleteAll();
        SaveData();
    }

    public void SaveData()
    {
        PlayerPrefs.Save();
    }

    public void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.INGAME:
                break;
            case GameState.GAMEOVER:
                SaveSongProgress();
                LoadSongInfo(CurrentSong);
                UpdateSongUI(CurrentSong, CurrentSongView);
                break;
            default:
                break;
        }
    }

    public void SaveSongProgress()
    {
        CurrentSong.level = KoreoManager.Instance.currentLevel;
        CurrentSong.levelEventsPercent = KoreoManager.Instance.GetEventsPercentPerSong();
        CurrentSong.levelLengthPercent = KoreoManager.Instance.GetLengthPercentPerSong();
        SetSongInfo(CurrentSong);
    }
}
