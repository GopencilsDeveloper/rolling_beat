﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Theme
{
    public Color topBG;
    public Color botBG;
    public Color note;
    public Color noteBorder;
    public Color ball;
    public Color trail;
    public Color line;
    public Texture road;
    public Sprite UIBG;
}
public class ThemeManager : MonoSingleton<ThemeManager>
{
    [Header("THEMES")]
    public Theme[] themes;

    [Header("SERIALIZED FIELDS")]
    public MeshRenderer BG;
    public MeshRenderer ball;
    public ParticleSystemRenderer trail;
    public LineRenderer line;
    public LineRenderer line1;
    public MeshRenderer road;
    public Image UIBG;

    [HideInInspector]
    public Theme currentTheme;

    public void ApplyTheme(int index)
    {
        currentTheme = themes[index];
        BG.material.SetColor("_Color", currentTheme.botBG);
        BG.material.SetColor("_Color2", currentTheme.topBG);
        ball.material.SetColor("_Color", currentTheme.ball);
        trail.material.SetColor("_TintColor", currentTheme.trail);
        SetLineRendererColorGradient(currentTheme.line, line);
        SetLineRendererColorGradient(currentTheme.line, line1);
        road.sharedMaterial.SetTexture("_MainTex", currentTheme.road);
        UIBG.sprite = currentTheme.UIBG;
    }

    private void SetLineRendererColorGradient(Color color, LineRenderer lr)
    {
        Gradient gradient = new Gradient();
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(1f, 0.0f), new GradientAlphaKey(0f, 1.0f) }
            );
        lr.colorGradient = gradient;
    }
}
