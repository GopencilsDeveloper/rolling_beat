﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using SonicBloom.Koreo;
using SonicBloom.Koreo.Players;
using NaughtyAttributes;

[System.Serializable]
public class Koreo
{
    public string eventID;
    public Koreography koreoGraphy;
}
public class KoreoManager : MonoSingleton<KoreoManager>
{
    #region Serialize Fields
    [Header("FIELDS")]
    public float timeToReachEvent;

    [Tooltip("The amount of time in seconds to provide before playback of the audio begins.  Changes to this value are not immediately handled during the lead-in phase while playing in the Editor.")]
    public float leadInTime;

    public float speedUpOffset;

    public ParticleSystem aura;
    #endregion

    #region Params
    private Player player;
    private SimpleMusicPlayer smp;
    private AudioSource audioSource;
    private Koreography playingKoreo;
    private string eventID;
    private KoreographyTrackBase rhythmTrack;
    private List<KoreographyEvent> rawEvents;
    public int pendingIndex = 0;
    private int delayedSample;
    private int sampleRate;
    private int startPipeSample;//Sample at start of pipe
    private int endPipeSample;//Sample at the end of pipe
    private float curveRotation = 0f;//Obstacles postion on pipe
    Pipe targetPipe;
    float targetCurveRotation;
    bool isGamePlaying = false;
    float leadInTimeLeft;   // The amount of leadInTime left before the audio is audible.
    float timeLeftToPlay;   // The amount of time left before we should play the audio (handles Event Delay).
    #endregion

    #region Properties
    public int SampleRate { get { return this.sampleRate; } }
    public AudioSource AudioSource { get { return this.audioSource; } }
    public string EventID { get { return this.eventID; } set { eventID = value; } }
    public Koreography PlayingKoreo { get { return playingKoreo; } }
    public int CurrentEventIndex { get { return pendingIndex; } }
    public int TotalEventsPerSong { get { return rawEvents.Count; } }

    [HideInInspector] public int missedScore = 0;
    [HideInInspector] public int scorePerSong = 0;
    public int currentLevel;
    public bool isContinued;

    // The current sample time, including any necessary delays.
    public int DelayedSampleTime
    {
        get
        {
            // Offset the time reported by Koreographer by a possible leadInTime amount.
            return playingKoreo.GetLatestSampleTime() - (int)(audioSource.pitch * leadInTimeLeft * SampleRate);
        }
    }
    #endregion

    #region Actions
    public static System.Action OnBeat;
    #endregion

    private void Awake()
    {
        smp = GetComponent<SimpleMusicPlayer>();
        audioSource = GetComponent<AudioSource>();
        audioSource.Stop();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        isGamePlaying = false;
        GameManager.GameStateChanged += OnGameStateChanged;
    }

    private void Start()
    {
        player.OnSetupCurrentPipe += GetPipeSample;
    }
    void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.MAINMENU:
                StopKoreo();
                break;
            case GameState.INGAME:
                scorePerSong = 0;
                InitializeKoreoGraphy(ItemsManager.Instance.CurrentSong);
                if (!isContinued)
                {
                    currentLevel = 0;
                    Start(currentLevel);
                }
                else
                {
                    currentLevel = ItemsManager.Instance.CurrentSong.level;
                    Continue(currentLevel);
                }
                Koreographer.Instance.RegisterForEvents(eventID, OnFireEvent);
                break;
            case GameState.RESTART:
                scorePerSong = 0;
                RestartKoreo(currentLevel);
                break;
            case GameState.PAUSE:
                PauseKoreo();
                break;
            case GameState.RESUME:
                ResumeKoreo();
                break;
            case GameState.GAMEOVER:
                StopKoreo();
                break;
            case GameState.REVIVE:
                PauseKoreo();
                break;
            default:
                break;
        }
    }

    public void InitializeKoreoGraphy(Song song)
    {
        playingKoreo = song.koreo.koreoGraphy;
        eventID = song.koreo.eventID;

        smp.LoadSong(playingKoreo, 0, false);
        rhythmTrack = playingKoreo.GetTrackByID(eventID);
        rawEvents = rhythmTrack.GetAllEvents();
        sampleRate = playingKoreo.SampleRate;
    }


    public void Start(int level)
    {
        audioSource.timeSamples = 0;
        pendingIndex = 0;

        InitializeLeadIn();
        audioSource.Stop();
        audioSource.pitch = 1 + level * speedUpOffset;
        GameManager.Instance.SetSpeed(level);
        pendingIndex = 0;

        Koreographer.Instance.FlushDelayQueue(playingKoreo);
        playingKoreo.ResetTimings();
        isGamePlaying = true;
        player.StartRoll();
    }

    public void Continue(int level)
    {
        if (ItemsManager.Instance.CurrentSong.CheckPointPercent == 0f)
        {
            pendingIndex = 0;
            audioSource.timeSamples = 0;
        }
        else
        {
            audioSource.timeSamples = rawEvents[GetIndexAtCheckedPoint()].StartSample + (int)leadInTime * sampleRate;
            pendingIndex = GetIndexAtCheckedPoint();
        }

        InitializeLeadIn();
        audioSource.Stop();
        audioSource.pitch = 1 + level * speedUpOffset;
        GameManager.Instance.SetSpeed(level);
        isGamePlaying = true;
        player.StartRoll();
    }

    public void RestartKoreo(int level)
    {
        audioSource.timeSamples = 0;
        InitializeLeadIn();
        audioSource.Stop();
        audioSource.pitch = 1 + level * speedUpOffset;
        GameManager.Instance.SetSpeed(level);
        pendingIndex = 0;

        Koreographer.Instance.FlushDelayQueue(playingKoreo);
        playingKoreo.ResetTimings();
        isGamePlaying = true;
        player.StartRoll();
    }

    public void PauseKoreo()
    {
        isGamePlaying = false;
        audioSource.Pause();
    }
    public void ResumeKoreo()
    {
        isGamePlaying = true;
        audioSource.UnPause();
    }
    public void StopKoreo()
    {
        isGamePlaying = false;
        audioSource.Stop();
    }

    // Sets up the lead-in-time.  Begins audio playback immediately if the specified lead-in-time is zero.
    void InitializeLeadIn()
    {
        // Initialize the lead-in-time only if one is specified.
        if (leadInTime > 0f)
        {
            // Set us up to delay the beginning of playback.
            leadInTimeLeft = leadInTime;
            timeLeftToPlay = leadInTime - Koreographer.Instance.EventDelayInSeconds;
        }
        else
        {
            // Play immediately and handle offsetting into the song.  Negative zero is the same as
            //  zero so this is not an issue.
            audioSource.time = -leadInTime;
        }
    }
    public void LeadInTimeToPlay()
    {
        // Count down some of our lead-in-time.
        if (leadInTimeLeft > 0f)
        {
            leadInTimeLeft = Mathf.Max(leadInTimeLeft - Time.deltaTime, 0f);
        }
        // Count down the time left to play, if necessary.
        if (timeLeftToPlay > 0f)
        {
            timeLeftToPlay -= Time.deltaTime;
            // Check if it is time to begin playback.
            if (timeLeftToPlay <= 0f)
            {
                audioSource.Play();
                timeLeftToPlay = 0f;
            }
        }
    }

    public int GetIndexAtCheckedPoint()
    {
        int[] arraySample = new int[rawEvents.Count];
        for (int i = 0; i < arraySample.Length; i++)
        {
            arraySample[i] = rawEvents[i].StartSample;
        }
        int temp = (int)(audioSource.clip.length * ItemsManager.Instance.CurrentSong.CheckPointPercent * sampleRate);
        return LeftSegmentIndex(arraySample, temp);

    }
    public static int LeftSegmentIndex(int[] array, int t)
    {
        int index = System.Array.BinarySearch(array, t);
        if (index < 0)
        {
            index = ~index - 1;
        }
        return System.Math.Min(System.Math.Max(index, 0), array.Length - 2);
    }

    public float GetAngularVelocity()
    {
        return player.Velocity * 360f / (2f * Mathf.PI * player.CurrentPipe.CurveRadius);
    }

    void GetPipeSample()
    {
        startPipeSample = DelayedSampleTime;
        endPipeSample = (int)((player.CurrentPipe.CurveAngle * sampleRate) / GetAngularVelocity()) + startPipeSample;
    }

    private void SpawnNote()
    {
        int currTimeInSample;
        currTimeInSample = DelayedSampleTime;
        float offsetDelaySample = timeToReachEvent * sampleRate;

        while (pendingIndex < rawEvents.Count &&
        rawEvents[pendingIndex].StartSample < currTimeInSample + offsetDelaySample)
        {
            KoreographyEvent currentNote = rawEvents[pendingIndex];
            float targetSample = currTimeInSample + offsetDelaySample;
            float offsetSample = targetSample - startPipeSample;

            curveRotation = (float)(offsetSample / sampleRate) *
            (float)(player.Velocity * 360f / (2f * Mathf.PI * player.CurrentPipe.CurveRadius));

            if (curveRotation < player.CurrentPipe.CurveAngle)
            {
                //Calculate Obstacles in range of current pipe.
                targetPipe = player.CurrentPipe;
                targetCurveRotation = curveRotation;
            }
            else
            {
                //Obstacles out of range form previous pipe.
                //Calculate Obstacles from previous pipe to next pipe.
                float nextCurveRotation = ((targetSample - endPipeSample) / sampleRate) *
                (float)(player.Velocity * 360f / (2f * Mathf.PI * player.NextPipe.CurveRadius));

                targetPipe = player.NextPipe;
                targetCurveRotation = nextCurveRotation;
            }

            // if (pendingIndex < rawEvents.Count - 1)
            // {
                // Spawn Notes
                if (currentNote.GetTextValue().Trim() == "1")
                {
                    //Left spiral
                    SpawnManager.Instance.SpawnNote(targetPipe, targetCurveRotation, true, -1, 3f);
                }
                else
                if (currentNote.GetTextValue().Trim() == "2")
                {
                    //Right spiral
                    SpawnManager.Instance.SpawnNote(targetPipe, targetCurveRotation, true, 1, 3f);
                }
                else
                if (currentNote.GetTextValue().Trim() == "1A")
                {
                    SpawnManager.Instance.SpawnNote(targetPipe, targetCurveRotation, true, -1, 4f);
                }
                else
                if (currentNote.GetTextValue().Trim() == "2A")
                {
                    SpawnManager.Instance.SpawnNote(targetPipe, targetCurveRotation, true, 1, 4f);
                }
                else 
                if (currentNote.GetTextValue().Trim() == "3")
                {
                    SpawnManager.Instance.SpawnNote(targetPipe, targetCurveRotation, true, 1, 20f);
                }
                else
                {
                    //Random
                    SpawnManager.Instance.SpawnNote(targetPipe, targetCurveRotation);
                }
            // }
            // else
            // if (pendingIndex == rawEvents.Count - 1)
            // {
            //     SpawnManager.Instance.SpawnGate(targetPipe);
            // }

            pendingIndex++;

        }
    }
    private void Update()
    {
        if (isGamePlaying == true)
        {
            LeadInTimeToPlay();
            SpawnNote();
            if (pendingIndex == rawEvents.Count)
            {
                if (!smp.IsPlaying)
                {
                    FacebookAnalytics.Instance.LogGame_end_levelsEvent(1);
                    currentLevel++;
                    ItemsManager.Instance.CurrentSong.levelEventsPercent = 0f;
                    ItemsManager.Instance.CurrentSong.levelLengthPercent = 0f;
                    UIManager.Instance.ShowSpeedUpPanel(currentLevel);
                    Start(currentLevel);
                    scorePerSong = 0;
                }
            }
        }
    }

    public float GetEventsPercentPerSong()
    {
        return (float)((float)scorePerSong / (float)TotalEventsPerSong);
    }

    public float GetLengthPercentPerSong()
    {
        if (GameManager.Instance.currentState == GameState.MAINMENU)
        {
            return 0;
        }
        else
        {
            return (float)DelayedSampleTime / (float)rawEvents[rawEvents.Count - 1].StartSample;
        }
    }

    public void OnFireEvent(KoreographyEvent evt)
    {
        aura.Play();
    }
}
