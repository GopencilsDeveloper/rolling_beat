﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoSingleton<AudioManager> {

	public AudioClip coinSFX;
	public AudioClip touchSFX;
	private AudioSource audioSource;
	private void Awake() {
		audioSource = GetComponent<AudioSource>();
		if (!audioSource)
		{
			gameObject.AddComponent<AudioSource>();
		}
	}

	public void PlayCoinSound()
	{
		audioSource.PlayOneShot(coinSFX);
	}
	public void PlayTouchSound()
	{
		audioSource.PlayOneShot(touchSFX);
	}
}
