﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
#if UNITY_ADS
using UnityEngine.Advertisements;
#endif

public enum GameState
{
    NULL, MAINMENU, INGAME, PAUSE, RESTART, RESUME, GAMEOVER, REVIVE
}
public class GameManager : MonoSingleton<GameManager>
{
    public Player player;
    public GameObject space;
    public GameState currentState;
    public static event System.Action<GameState> GameStateChanged;
    private float timeScaleBeforePause;

    public override void Initialize()
    {
        base.Initialize();
        GameStateChanged += OnGameStateChanged;
    }
    private void Start()
    {
        MainMenuGame();
        Application.targetFrameRate = 60;
    }
    public void ChangeGameState(GameState state)
    {
        currentState = state;
        if (GameStateChanged != null)
        {
            GameStateChanged(state);
        }
    }
    public void OnReWardedVideoCompleted()
    {
        if (currentState == GameState.REVIVE)
        {
            ResumeGame();
        }
    }
    public void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.MAINMENU:
                Time.timeScale = 1f;
                space.SetActive(false);
                break;
            case GameState.INGAME:
                space.SetActive(true);
                break;
            case GameState.RESTART:
                Time.timeScale = 1f;
                break;
            case GameState.PAUSE:
                timeScaleBeforePause = Time.timeScale;
                Time.timeScale = 0f;
                break;
            case GameState.RESUME:
                Time.timeScale = timeScaleBeforePause;
                break;
            case GameState.GAMEOVER:
                // UnityAdsManager.Instance.ShowDefaultAds("", true);
                break;
            case GameState.REVIVE:
                timeScaleBeforePause = Time.timeScale;
                Time.timeScale = 0f;
                break;
            default:
                break;
        }
    }
    public void SetSpeed(int level)
    {
        Time.timeScale = 1 + level * KoreoManager.Instance.speedUpOffset;
    }
    public void MainMenuGame()
    {
        ChangeGameState(GameState.MAINMENU);
    }
    public void PlayGame()
    {
        ChangeGameState(GameState.INGAME);
        FacebookAnalytics.Instance.LogGame_startEvent(1);
        GameAnalyticsManager.Instance.Log_StartGame("Start");
    }
    public void RestartGame()
    {
        ChangeGameState(GameState.RESTART);
        GameAnalyticsManager.Instance.Log_StartGame("Restart");
    }
    public void PauseGame()
    {
        ChangeGameState(GameState.PAUSE);
    }
    public void ResumeGame()
    {
        ChangeGameState(GameState.RESUME);
    }
    public void GameOver()
    {
        ChangeGameState(GameState.GAMEOVER);
        FacebookAnalytics.Instance.LogGame_endEvent(1);
        GameAnalyticsManager.Instance.Log_EndGame("GameOver");
    }
    public void ReviveGame()
    {
        ChangeGameState(GameState.REVIVE);
        FacebookAnalytics.Instance.LogGame_end_endlessEvent(1);
        GameAnalyticsManager.Instance.Log_EndGame("Die");
    }
    public void DelayResume()
    {
        player.objPlayerCollideWith.SetActive(false);
        Time.timeScale = timeScaleBeforePause;
        StartCoroutine(C_DelayResume(3f));
        GameAnalyticsManager.Instance.Log_StartGame("Revive");
    }
    IEnumerator C_DelayResume(float duration)
    {
        yield return new WaitForSeconds(duration);
        ResumeGame();
    }
}
