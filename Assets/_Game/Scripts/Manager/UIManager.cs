﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using DG.Tweening;
using NaughtyAttributes;

public class UIManager : MonoSingleton<UIManager>
{
    public Color playerColor;

    [Header("LINK STORES")]
    const string APPLESTORE_LINK = "itms-apps://itunes.apple.com/app/id";
    public string APPSTORE_ID = "123456789";

    [Header("PAGES")]
    public GameObject[] pages;
    public GameObject obstacleUI;

    [Header("MAIN CAMERA")]
    public Camera mainCamera;

    [ReorderableList]
    public Color[] cameraColors;

    [Header("MAIN MENU")]
    public GameObject mainMenu;
    public GameObject continueScreen;
    public Button playBtn;
    public Button resetBtn;
    public Button continueBtn;
    public HorizontalScrollSnap horizontalScrollSnap;
    public Button watchAdsBtn;

    [Header("INGAME")]
    public GameObject inGame;
    public Button restartBtn;
    public Text distanceTxt;
    public GameObject speedUpPanel;
    public Text levelValueTxt;
    public Image starsPercent;
    public Image inGameLevelLengthPercent;
    public GameObject countDownPanel;
    public Text countDownTxt;
    public GameObject removeAdsItem;
    public Button removeAdsBtn;
    public GameObject perfect;
    public GameObject miss;
    public Text missedValueText;
    [HideInInspector] public int missedScore = 0;
    public ParticleSystem completeLevel;
    public Transform lengthPecentTrans;
    public Text[] percents;
    public Text introLevelTxt;

    [Header("PAUSE")]
    public GameObject pauseMenu;
    public Button pauseBtn;
    public Button resumeBtn;
    public Button homeBtn;

    [Header("GAME OVER")]
    public GameObject gameOver;
    public Text nameSongTxt;
    public Image levelPercent;
    public Button homeBtn1;
    public Button restartBtn1;
    public Text levelGameOverTxt;
    public Text distanceValueTxt;
    public Text totalGemTxt;
    public Text perfectTxt;
    public Image levelLengthPercent;
    public Text levelLengthPercentTxt;

    [Header("REVIVE")]
    public GameObject reviveMenu;
    public Button cancelReviveBtn;
    public Button watchAdsReviveBtn;
    public Button chargeGemReviveBtn;
    public Text revivePrice;
    public Text reviveNameSongTxt;
    public Image revivePercent;
    public Text predictPercentTxt;
    public Text levelLengthPercentTxt1;

    [Header("BASE")]
    public Text scoreTxt;

    [Header("RATING")]
    public GameObject ratingMenu;
    public Button OKBtn;
    public Button notNowBtn;
    const string RATINGCOUNT = "RatingCount";

    [Header("PLAYER")]
    public Player player;

    #region Params
    private float starsPercentValue;
    public float StarsPercentValue { get { return starsPercentValue; } }
    public int CurrentSongIndex { get { return this.horizontalScrollSnap._currentPage; } }
    #endregion

    private void Awake()
    {
        GameManager.GameStateChanged += OnGameStateChanged;
    }

    // /// <summary>
    // /// Your IStoreListener implementation of OnInitialized.
    // /// </summary>
    // public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    // {
    //     extensions.GetExtension<IAppleExtensions>().RestoreTransactions(result =>
    //     {
    //         if (result)
    //         {
    //             // This does not mean anything was restored,
    //             // merely that the restoration process succeeded.
    //             print("Can be restored.");
    //         }
    //         else
    //         {
    //             // Restoration failed.
    //             print("Restoration failed.");
    //         }
    //     });
    // }

    private void Start()
    {
        ChangeTheme(0);
        RegisterEvents();
        if (!mainCamera)
        {
            mainCamera = Camera.main;
        }
        horizontalScrollSnap.OnSelectionPageChangedEvent.AddListener(ChangeTheme);
        horizontalScrollSnap.OnSelectionPageChangedEvent.AddListener(CheckPlayable);
        horizontalScrollSnap.OnSelectionPageChangedEvent.AddListener(CheckBuyable);
        SpawnObstacleUI();


        PlayerPrefs.SetInt(RATINGCOUNT, PlayerPrefs.GetInt(RATINGCOUNT) + 1);
        if (IsPowerOfTwo(PlayerPrefs.GetInt(RATINGCOUNT)) && PlayerPrefs.GetInt(RATINGCOUNT) > 1)
        {
            ratingMenu.SetActive(true);
        }
        else
        {
            ratingMenu.SetActive(false);
        }
    }

    private void Update()
    {
        if (GameManager.Instance.currentState == GameState.INGAME ||
            GameManager.Instance.currentState == GameState.RESUME ||
            GameManager.Instance.currentState == GameState.RESTART)
        {
            UpdateLengthPercent();
        }
        DoLengthBar();
    }

    void DoLengthBar()
    {
        if (!isScaled)
        {
            switch ((int)(KoreoManager.Instance.GetLengthPercentPerSong() * 100f))
            {
                case 25:
                    percents[0].gameObject.SetActive(true);
                    DoScaleLengthBar();
                    percents[0].DOText("25%", 1f, true, ScrambleMode.All).OnComplete(() => { percents[0].gameObject.SetActive(false); });
                    break;
                case 50:
                    percents[1].gameObject.SetActive(true);
                    DoScaleLengthBar();
                    percents[1].DOText("50%", 1f, true, ScrambleMode.All).OnComplete(() => { percents[1].gameObject.SetActive(false); });
                    break;
                case 75:
                    percents[2].gameObject.SetActive(true);
                    DoScaleLengthBar();
                    percents[2].DOText("75%", 1f, true, ScrambleMode.All).OnComplete(() => { percents[2].gameObject.SetActive(false); });
                    break;
                default:
                    break;
            }
        }
        if (((int)(KoreoManager.Instance.GetLengthPercentPerSong() * 100f) == 26f
        || (int)(KoreoManager.Instance.GetLengthPercentPerSong() * 100f) == 51f
        || (int)(KoreoManager.Instance.GetLengthPercentPerSong() * 100f) == 76f))
        {
            isScaled = false;
        }
    }

    bool isScaled = false;

    void DoScaleLengthBar()
    {
        lengthPecentTrans.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.2f).OnComplete(() => lengthPecentTrans.DOScale(new Vector3(1f, 1f, 1f), 0.2f));
        isScaled = true;
    }

    public void CheckPlayable(int pageNo)
    {
        playBtn.gameObject.SetActive(ItemsManager.Instance.songs[pageNo].isUnlocked == 1);
    }

    public void CheckBuyable(int pageNo)
    {
        ItemsManager.Instance.CurrentSongView.CheckBuyable();
    }

    bool IsPowerOfTwo(int x)
    {
        return (x != 0) && ((x & (x - 1)) == 0);
    }

    public void SpawnObstacleUI()
    {
        for (int i = 0; i < 15; i++)
        {
            Instantiate(obstacleUI);
        }
    }

    private void ChangeCameraColor(int pageNo)
    {
        if (pageNo >= cameraColors.Length)
        {
            pageNo -= cameraColors.Length;
        }
        mainCamera.backgroundColor = cameraColors[pageNo];
    }

    private void ChangeTheme(int pageNo)
    {
        if (pageNo >= ThemeManager.Instance.themes.Length)
        {
            pageNo -= ThemeManager.Instance.themes.Length;
        }
        ThemeManager.Instance.ApplyTheme(pageNo);
    }

    public void ShowPage(string pageTag)
    {
        foreach (var page in pages)
        {
            if (page.CompareTag(pageTag))
            {
                page.SetActive(true);
            }
            else
            {
                page.SetActive(false);
            }
        }
    }

    public void ShowSpeedUpPanel(int index)
    {
        completeLevel.Play();
        if (index >= GameUltility.Instance.nameLevels.Length)
        {
            index = GameUltility.Instance.nameLevels.Length;
        }
        speedUpPanel.SetActive(true);
        levelValueTxt.text = GameUltility.Instance.nameLevels[index];
        levelValueTxt.color = GameUltility.Instance.levelColor[index];

        StartCoroutine(C_HideObj(speedUpPanel, 3f));
    }
    IEnumerator C_HideObj(GameObject obj, float delay)
    {
        yield return new WaitForSeconds(delay);
        obj.SetActive(false);
    }
    public void UpdateScoreUI()
    {
        scoreTxt.DOText(ScoreManager.Instance.totalScore.ToString(), 1f, false, ScrambleMode.Numerals);
    }

    float t = 0;
    IEnumerator C_InceaseValue(string value, float min, float max, float duration)
    {
        t += Time.deltaTime / duration;
        while (t < 0)
        {
            value = Mathf.Lerp(min, max, t).ToString();
            yield return null;
        }
    }

    void OnGameStateChanged(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.MAINMENU:
                missedScore = 0;
                mainMenu.SetActive(true);
                continueScreen.SetActive(false);
                inGame.SetActive(false);
                pauseMenu.SetActive(false);
                gameOver.SetActive(false);
                reviveMenu.SetActive(false);
                UpdateScoreUI();
                break;
            case GameState.INGAME:
                UpdatePercent();
                UpdateLengthPercent();
                mainMenu.SetActive(false);
                inGame.SetActive(true);
                pauseMenu.SetActive(false);
                gameOver.SetActive(false);
                ShowIntroLevel();
                break;
            case GameState.RESTART:
                UpdatePercent();
                UpdateLengthPercent();
                missedScore = 0;
                inGame.SetActive(true);
                pauseMenu.SetActive(false);
                gameOver.SetActive(false);
                break;
            case GameState.PAUSE:
                inGame.SetActive(false);
                pauseMenu.SetActive(true);
                break;
            case GameState.RESUME:
                inGame.SetActive(true);
                pauseMenu.SetActive(false);
                reviveMenu.SetActive(false);
                break;
            case GameState.GAMEOVER:
                ShowGameOverMenu();
                UpdateScoreUI();
                reviveMenu.SetActive(false);
                pauseMenu.SetActive(false);
                if (KoreoManager.Instance.currentLevel >= 5)
                {
                    ratingMenu.SetActive(true);
                }
                else
                {
                    ratingMenu.SetActive(false);
                }
                break;
            case GameState.REVIVE:
                ShowReviveMenu();
                break;
            default:
                break;
        }
    }

    void ShowIntroLevel()
    {
        introLevelTxt.gameObject.SetActive(true);
        introLevelTxt.text = GameUltility.Instance.IntToNameLevel(KoreoManager.Instance.currentLevel);
        introLevelTxt.color = GameUltility.Instance.IntToColorLevel(KoreoManager.Instance.currentLevel);
        introLevelTxt.gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        introLevelTxt.gameObject.transform.DOScale(Vector3.one, 1.0f).OnComplete(() => { introLevelTxt.gameObject.SetActive(false); });
    }

    void ShowReviveMenu()
    {
        revivePrice.text = ScoreManager.Instance.reviveGemPrice.ToString();
        reviveNameSongTxt.text = ItemsManager.Instance.CurrentSong.nameSong;
        revivePercent.fillAmount = KoreoManager.Instance.GetLengthPercentPerSong();
        levelLengthPercentTxt1.text = string.Format("{0:0.0}", (KoreoManager.Instance.GetLengthPercentPerSong() * 100f)) + "%";

        float predictPercent;
        if (KoreoManager.Instance.GetLengthPercentPerSong() >= 0f && KoreoManager.Instance.GetLengthPercentPerSong() < 0.25f)
        {
            predictPercent = (Mathf.Abs(0.25f - KoreoManager.Instance.GetLengthPercentPerSong()) * 100f);
            predictPercentTxt.text = string.Format("{0:0}", predictPercent) + "%";
        }
        else
        if (KoreoManager.Instance.GetLengthPercentPerSong() >= 0.25f && KoreoManager.Instance.GetLengthPercentPerSong() < 0.5f)
        {
            predictPercent = (Mathf.Abs(0.5f - KoreoManager.Instance.GetLengthPercentPerSong()) * 100f);
            predictPercentTxt.text = string.Format("{0:0}", predictPercent) + "%";
        }
        else
        if (KoreoManager.Instance.GetLengthPercentPerSong() >= 0.5f && KoreoManager.Instance.GetLengthPercentPerSong() < 0.75f)
        {
            predictPercent = (Mathf.Abs(0.75f - KoreoManager.Instance.GetLengthPercentPerSong()) * 100f);
            predictPercentTxt.text = string.Format("{0:0}", predictPercent) + "%";
        }
        else
        if (KoreoManager.Instance.GetLengthPercentPerSong() >= 0.75f && KoreoManager.Instance.GetLengthPercentPerSong() < 0.99f)
        {
            predictPercent = (Mathf.Abs(1f - KoreoManager.Instance.GetLengthPercentPerSong()) * 100f);
            predictPercentTxt.text = string.Format("{0:0}", predictPercent) + "%";
        }

        if (!UnityAdsManager.Instance.isAdsReady)
        {
            watchAdsReviveBtn.interactable = false;
        }
        if (ScoreManager.Instance.totalScore < ScoreManager.Instance.reviveGemPrice)
        {
            chargeGemReviveBtn.interactable = false;
        }
        reviveMenu.SetActive(true);

    }

    public void SetDistance(float distance)
    {
        distanceTxt.text = ((int)(distance * 0.02f)).ToString() + "m";
        distanceValueTxt.text = ((int)(distance * 0.02f)).ToString() + "m";
    }

    public void ShowGameOverMenu()
    {
        nameSongTxt.text = ItemsManager.Instance.CurrentSong.nameSong;
        levelPercent.fillAmount = KoreoManager.Instance.GetEventsPercentPerSong();
        levelLengthPercent.fillAmount = KoreoManager.Instance.GetLengthPercentPerSong();
        levelLengthPercentTxt.text = string.Format("{0:0.0}", (KoreoManager.Instance.GetLengthPercentPerSong() * 100f)) + "%";
        levelGameOverTxt.text = GameUltility.Instance.IntToNameLevel(KoreoManager.Instance.currentLevel);
        levelGameOverTxt.color = GameUltility.Instance.IntToColorLevel(KoreoManager.Instance.currentLevel);
        totalGemTxt.text = ScoreManager.Instance.inGameScore.ToString();
        perfectTxt.text = totalGemTxt.text;
        missedValueText.text = missedScore.ToString();
        gameOver.SetActive(true);
    }

    private void RegisterEvents()
    {
        playBtn.onClick.AddListener(OnPlayBtnClicked);
        continueBtn.onClick.AddListener(OnContinueBtnClicked);
        resetBtn.onClick.AddListener(OnResetBtnClicked);
        restartBtn.onClick.AddListener(OnRestartBtnClicked);
        restartBtn1.onClick.AddListener(OnRestartBtnClicked);
        pauseBtn.onClick.AddListener(OnPauseBtnClicked);
        resumeBtn.onClick.AddListener(OnResumeBtnClicked);
        homeBtn.onClick.AddListener(OnHomeBtnClicked);
        homeBtn1.onClick.AddListener(OnHomeBtnClicked);
        watchAdsReviveBtn.onClick.AddListener(OnReviveBtnClicked);
        chargeGemReviveBtn.onClick.AddListener(OnGemReviveBtnClicked);
        cancelReviveBtn.onClick.AddListener(OnCancelReviveBtnClicked);
        watchAdsBtn.onClick.AddListener(OnWatchAdsBtnClicked);
    }

    public void OnRemoveAdsBtnClicked()
    {
        UnityAdsManager.Instance.RemoveAds();
        removeAdsItem.SetActive(false);
    }
    private void OnPlayBtnClicked()
    {
        if (ItemsManager.Instance.CurrentSong.IsCheckedPoint)
        {
            continueScreen.SetActive(true);
        }
        else
        {
            OnResetBtnClicked();
        }
    }
    private void OnContinueBtnClicked()
    {
        KoreoManager.Instance.isContinued = true;
        GameManager.Instance.PlayGame();
    }
    private void OnResetBtnClicked()
    {
        KoreoManager.Instance.isContinued = false;
        GameManager.Instance.PlayGame();
    }
    private void OnRestartBtnClicked()
    {
        GameManager.Instance.RestartGame();
    }
    private void OnPauseBtnClicked()
    {
        GameManager.Instance.PauseGame();
    }
    private void OnResumeBtnClicked()
    {
        GameManager.Instance.ResumeGame();
    }
    private void OnHomeBtnClicked()
    {
        GameManager.Instance.MainMenuGame();
    }
    private void OnReviveBtnClicked()
    {
        UnityAdsManager.Instance.ShowRewardedAds();
    }
    private void OnGemReviveBtnClicked()
    {
        ScoreManager.Instance.MinusScore(ScoreManager.Instance.reviveGemPrice);
        GameManager.Instance.DelayResume();
        UIManager.Instance.HideReviveMenu();
    }
    private void OnCancelReviveBtnClicked()
    {
        GameManager.Instance.GameOver();
    }

    public void OnOkRatingBtnClicked()
    {
        RateUs();
    }
    public void OnCancelRatingBtnClicked()
    {
        ratingMenu.SetActive(false);
    }

    public void UpdatePercent()
    {
        starsPercentValue = KoreoManager.Instance.GetEventsPercentPerSong();
        starsPercent.fillAmount = starsPercentValue;
    }
    public void UpdateLengthPercent()
    {
        inGameLevelLengthPercent.fillAmount = KoreoManager.Instance.GetLengthPercentPerSong();
    }
    public void HideReviveMenu()
    {
        reviveMenu.SetActive(false);
        countDownPanel.gameObject.SetActive(true);
        StartCoroutine(C_CountDown(3f));
    }
    IEnumerator C_CountDown(float duration)
    {
        countDownTxt.text = duration.ToString();
        while (duration > 0)
        {
            yield return new WaitForSeconds(1);
            duration--;
            countDownTxt.text = duration.ToString();
        }
        countDownPanel.gameObject.SetActive(false);
    }

    public void OnWatchAdsBtnClicked()
    {
        UnityAdsManager.Instance.ShowRewardedAds();
    }

    public void PlayPerfectAnim()
    {
        perfect.SetActive(false);
        perfect.SetActive(true);

        StartCoroutine(C_HideObj(perfect, 0.25f));
    }

    public void PlayMissAnim()
    {
        miss.SetActive(false);
        miss.SetActive(true);

        StartCoroutine(C_HideObj(miss, 0.25f));
    }

    public void RateUs()
    {
#if UNITY_IOS
        Application.OpenURL(APPLESTORE_LINK + APPSTORE_ID);
#endif
    }

    public void PromoGame(string gameID)
    {
#if UNITY_IOS
        Application.OpenURL(APPLESTORE_LINK + gameID);
#endif
    }

    public void AboutUs()
    {
        Application.OpenURL("https://www.facebook.com/GoPencils/");
    }
}
