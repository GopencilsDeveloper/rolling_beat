using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_ADS
using UnityEngine.Advertisements;
#endif

public class UnityAdsManager : MonoSingleton<UnityAdsManager>
{
    const string isRemovedAds = "isRemovedAds";
#if UNITY_ADS
    private string GooglePlayAdsID = "2750237";
    private string AppStoreAdsID = "2750239";
    private string gameID;

    private float timeLastAdsShow = 0;
    public float timeBetweenAds = 180;
    private bool firstTime = true;

    public bool isAdsReady { get { return Advertisement.IsReady(); } }

    // Use this for initialization
    void Awake()
    {
#if UNITY_ANDROID
        gameID = GooglePlayAdsID;
#elif UNITY_IOS
		gameID = AppStoreAdsID;
#endif
        Advertisement.Initialize(gameID);
    }

    public void RemoveAds()
    {
        PlayerPrefs.SetInt(isRemovedAds, 1);
    }

    private bool CanShowAds()
    {
        if (firstTime)
        {
            firstTime = false;
            timeLastAdsShow = Time.realtimeSinceStartup;

            return true;
        }
        else
        {
            float currentTime = Time.realtimeSinceStartup;

            if (currentTime - timeLastAdsShow >= timeBetweenAds)
            {
                timeLastAdsShow = currentTime;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public void ShowDefaultAds(string placementID = "", bool isDelayBetweenAds = true)
    {
        if (PlayerPrefs.GetInt(isRemovedAds) == 0)
        {
            StartCoroutine(WaitForAd());

            if (string.Equals(placementID, ""))
                placementID = null;

            ShowOptions options = new ShowOptions();
            options.resultCallback = AdCallbackhandler;
            if (Advertisement.IsReady(placementID))
            {
                if (isDelayBetweenAds)
                {
                    if (CanShowAds())
                    {
                        Advertisement.Show(placementID);
                        FacebookAnalytics.Instance.LogInterstitial_adsEvent(1);
                    }
                }
                else
                {
                    Advertisement.Show(placementID);
                    FacebookAnalytics.Instance.LogInterstitial_adsEvent(1);
                }
            }
        }
    }

    public void ShowRewardedAds()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = AdCallbackhandler };
            Advertisement.Show("rewardedVideo", options);
            FacebookAnalytics.Instance.LogReward_adsEvent(1);
        }
    }

    public static System.Action<ShowResult> OnReWardedVideoResultChanged;
    void AdCallbackhandler(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                if (GameManager.Instance.currentState == GameState.REVIVE)
                {
                    GameManager.Instance.DelayResume();
                    UIManager.Instance.HideReviveMenu();
                }
                if (GameManager.Instance.currentState == GameState.MAINMENU)
                {
                    ScoreManager.Instance.AddScore(10);
                }
                break;
            case ShowResult.Skipped:
                if (GameManager.Instance.currentState == GameState.REVIVE)
                {
                    GameManager.Instance.GameOver();
                }
                break;
            case ShowResult.Failed:
                if (GameManager.Instance.currentState == GameState.REVIVE)
                {
                    GameManager.Instance.GameOver();
                }
                break;
        }
    }

    IEnumerator WaitForAd()
    {
        float currentTimeScale = Time.timeScale;
        Time.timeScale = 0f;
        yield return null;

        while (Advertisement.isShowing)
            yield return null;

        Time.timeScale = currentTimeScale;
    }
#endif
}