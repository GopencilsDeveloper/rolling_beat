﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoSingleton<ObjectPooler>
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public List<GameObject> prefabs;
        public int size;
    }

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> poolDictionary;
    private void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (var pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                var prefabsbCount = pool.prefabs.Count;
                GameObject obj = Instantiate(pool.prefabs[Random.Range(0, prefabsbCount)]);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionary.Add(pool.tag, objectPool);
        }
    }
    public GameObject SpawnFromPool(string tag)
    {
        if (!poolDictionary.ContainsKey(tag))
        {
            print("Pool with tag " + tag + "doesn't exist");
            return null;
        }
        GameObject objToSpawn = poolDictionary[tag].Dequeue();

        poolDictionary[tag].Enqueue(objToSpawn);
        return objToSpawn;
    }
}
