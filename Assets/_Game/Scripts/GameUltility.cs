﻿using System.Collections;
using UnityEngine;
using NaughtyAttributes;

public class GameUltility : MonoSingleton<GameUltility>
{
    [ReorderableList]
    public string[] nameLevels;
    
    [ReorderableList]
    public Color[] levelColor;

    public float duration = 0.05f;
    public float maxSize = 1.5f;

    // This remains true while it is in the prossess of scaling.
    private bool isScaling = false;

    public void DoScale(Transform trans)
    {
        if (trans.gameObject.activeInHierarchy)
        {
            StartCoroutine(C_DoScale(trans));
        }
    }
    private IEnumerator C_DoScale(Transform trans)
    {
        Vector3 originScale = trans.localScale;
        // Check if we are scaling now, if so exit method to avoid overlap.
        if (isScaling)
            yield break;

        // Declare that we are scaling now.
        isScaling = true;

        // Grab the current time and store it in a variable.
        float startTime = Time.time;

        while (Time.time - startTime < duration)
        {
            float amount = (Time.time - startTime) / duration;
            trans.localScale = Vector3.Lerp(originScale, originScale * maxSize, amount);
            yield return null;
        }

        trans.localScale = originScale * maxSize;

        // Leave the scale at maxSize for few seconds (this can be changed at any time).
        // yield return new WaitForSeconds(0.1f);

        // Now for the scale down part.  Store the current time in the same variable.
        startTime = Time.time;

        while (Time.time - startTime < duration)
        {
            float amount = (Time.time - startTime) / duration;
            trans.localScale = Vector3.Lerp(originScale * maxSize, originScale, amount);
            yield return null;
        }

        trans.localScale = originScale;

        // Declare that we are no longer modifing the scale.
        isScaling = false;
    }

    public string IntToNameLevel(int index)
    {
        return nameLevels[index];
    }
    public Color IntToColorLevel(int index)
    {
        return levelColor[index];
    }
}
