﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetManager : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        NetworkManagerHUD hud = FindObjectOfType<NetworkManagerHUD>();
        if (hud != null)
            hud.showGUI = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
