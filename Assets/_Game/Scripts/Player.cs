﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public Transform trail;
    public PipeSystem pipeSystem;

    public float startVelocity;
    public float rotationVelocity;

    public MainMenu mainMenu;
    public HUD hud;
    // public float[] accelerations;

    private Pipe currentPipe;
    private Pipe nextPipe;
    public Pipe CurrentPipe { get { return this.currentPipe; } }
    public Pipe NextPipe { get { return this.nextPipe; } }
    public float Velocity { get { return this.velocity; } }

    private float acceleration, velocity;
    private float distanceTraveled;
    private float deltaToRotation;
    private float systemRotation;
    private float worldRotation;

    private Transform space, world;
    private Quaternion targetCameraLocalRotation;

    public event System.Action OnSetupCurrentPipe;

    public void StartGame(int accelerationMode)
    {
        distanceTraveled = 0f;
        systemRotation = 0f;
        worldRotation = 0f;
        // acceleration = accelerations[accelerationMode];
        velocity = startVelocity;
        currentPipe = pipeSystem.SetupFirstPipe();
        nextPipe = pipeSystem.nextPipe;
        SetupCurrentPipe();
        gameObject.SetActive(true);
        hud.SetValues(distanceTraveled, velocity);

        KoreoManager.Instance.Play();

        targetCameraLocalRotation = Camera.main.transform.localRotation;
    }

    public void Die()
    {
        mainMenu.EndGame(distanceTraveled);
        gameObject.SetActive(false);
    }

    private void Awake()
    {
        world = pipeSystem.transform.parent;
        space = world.transform.parent;
        gameObject.SetActive(false);
    }
    Quaternion temp;
    private void Update()
    {
        // velocity += acceleration * Time.deltaTime;
        float delta = velocity * Time.deltaTime;
        distanceTraveled += delta;
        systemRotation += delta * deltaToRotation;

        if (systemRotation >= currentPipe.CurveAngle)
        {
            delta = (systemRotation - currentPipe.CurveAngle) / deltaToRotation;
            currentPipe = pipeSystem.SetupNextPipe();
            nextPipe = pipeSystem.nextPipe;
            SetupCurrentPipe();
            systemRotation = delta * deltaToRotation;
            SpawnManager.Instance.SpawnRing(currentPipe);
        }
        // Camera.main.transform.localRotation =
            //   Quaternion.Lerp(Quaternion.Euler(-10, 115f, 0f), targetCameraLocalRotation, Time.time * 0.11f);

        pipeSystem.transform.localRotation = Quaternion.Euler(0f, 0f, systemRotation);

        UpdateRotation();

        hud.SetValues(distanceTraveled, velocity);
    }

    Vector2 beganPosition;
    private void UpdateRotation()
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                beganPosition = Input.mousePosition;
            }
            if (Input.GetMouseButton(0))
            {
                Vector2 currentPosition = Input.mousePosition;
                float xMovement = beganPosition.x - currentPosition.x;
                float yMovement = beganPosition.y - currentPosition.y;
                beganPosition = currentPosition;

                if (xMovement != 0.0f || yMovement != 0.0f)
                {
                    // Debug.Log(xMovement + ", " + yMovement);
                    space.Rotate(Vector3.right, -xMovement * 0.35f);
                }
            }
            else
            {
                space.Rotate(Vector3.right * Input.GetAxis("Horizontal") * 5f);
                // Camera.main.transform.position -= new Vector3(0, 0, -Input.GetAxis("Horizontal") * 0.01f);
                // Camera.main.transform.eulerAngles -= new Vector3(0, -Input.GetAxis("Horizontal") * 0.2f, 0);
                // trail.Rotate(Vector3.up * Input.GetAxis("Horizontal") * 0.2f);
            }
        }
        if (Application.isMobilePlatform)
        {
            Touch touch = Input.GetTouch(0);
            if (Input.touchCount >= 1)
            {
                // float swipeDistance = touch.position.x - beganPosition.x;
                if (touch.phase == TouchPhase.Began)
                {
                    beganPosition = touch.position;
                }
                if (touch.phase == TouchPhase.Moved)
                {
                    space.Rotate(Vector3.right, touch.deltaPosition.x * 0.2f);
                    // Camera.main.transform.position += new Vector3(0, 0, touch.deltaPosition.x * 0.0005f);
                    // Camera.main.transform.eulerAngles += new Vector3(0, touch.deltaPosition.x * 0.0005f, 0);
                }
            }
        }

    }
    private void SetupCurrentPipe()
    {
        deltaToRotation = 360f / (2f * Mathf.PI * currentPipe.CurveRadius);
        worldRotation += currentPipe.RelativeRotation;
        if (worldRotation < 0f)
        {
            worldRotation += 360f;
        }
        else if (worldRotation >= 360f)
        {
            worldRotation -= 360f;
        }
        world.localRotation = Quaternion.Euler(worldRotation, 0f, 0f);
        OnSetupCurrentPipe();
    }
}