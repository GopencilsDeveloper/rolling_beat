﻿using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LoadSceneController : MonoBehaviour
{
    public GameObject loadingScreen;
    public Slider loadingSlider;

    private void Start()
    {
		LoadScene(1);
    }
    IEnumerator C_LoadAsync(int sceneIndex, float delay)
    {
        yield return new WaitForSeconds(delay);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);
        loadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            loadingSlider.value = progress;
            yield return null;
        }
    }

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(C_LoadAsync(sceneIndex, 3f));
    }

}
