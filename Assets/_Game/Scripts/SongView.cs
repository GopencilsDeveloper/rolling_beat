﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SongView : MonoBehaviour
{
    public GameObject unLocked;
    public GameObject locked;

    public Text STT;
    public Text title;
    public Text priceTxt;
    public Text levelName;
    public Image levelLengthPercentImage;
    public Text levelLengthPercentTxt;
    public Image levelPercentImage;
    public Button buyBtn;

    private int price;
    private string itemID;
    [HideInInspector] public Song song;
    public void Initialize(
        int order,
        Song song,
        string itemID,
        int price,
        int isUnlocked,
        string nameSong,
        int level,
        float levelPercent,
        float levelLengthPercent
        )
    {
        this.song = song;
        this.itemID = itemID;
        this.STT.text = order.ToString();
        this.title.text = nameSong;
        this.price = price;
        this.priceTxt.text = this.price.ToString();

        levelPercent = Mathf.Clamp(levelPercent, 0f, 1f);
        levelLengthPercent = Mathf.Clamp(levelLengthPercent, 0f, 1f);

        this.levelPercentImage.fillAmount = levelPercent;
        this.levelLengthPercentImage.fillAmount = levelLengthPercent;
        this.levelLengthPercentTxt.text = string.Format("{0:0.0}", (levelLengthPercent * 100f)) + "%";
        this.levelName.text = GameUltility.Instance.IntToNameLevel(level);
        this.levelName.color = GameUltility.Instance.IntToColorLevel(level);

        this.unLocked.SetActive(isUnlocked == 1);
        this.locked.SetActive(isUnlocked == 0);
        this.buyBtn.interactable = (price <= ScoreManager.Instance.TotalScore);
    }

    private void Start()
    {
        buyBtn.onClick.AddListener(OnBuyBtnClicked);
    }

    public void CheckBuyable()
    {
        this.buyBtn.interactable = (price <= ScoreManager.Instance.TotalScore);
    }

    private void OnBuyBtnClicked()
    {
        ScoreManager.Instance.MinusScore(price);
        ItemsManager.Instance.UnlockItem(this.itemID);
        ItemsManager.Instance.LoadSongInfo(this.song);
        UIManager.Instance.CheckPlayable(UIManager.Instance.CurrentSongIndex);
        UpdateSongView(this.song);
        AudioManager.Instance.PlayTouchSound();
    }

    public void UpdateSongView(Song song)
    {
        this.song = song;
        song.levelEventsPercent = Mathf.Clamp(song.levelEventsPercent, 0f, 1f);
        song.levelLengthPercent = Mathf.Clamp(song.levelLengthPercent, 0f, 1f);
        this.levelPercentImage.fillAmount = song.levelEventsPercent;
        this.levelLengthPercentImage.fillAmount = song.levelLengthPercent;
        this.levelLengthPercentTxt.text = string.Format("{0:0.0}", (song.levelLengthPercent * 100f)) + "%";
        this.levelName.text = GameUltility.Instance.IntToNameLevel(song.level);
        this.levelName.color = GameUltility.Instance.IntToColorLevel(song.level);

        this.unLocked.SetActive(song.isUnlocked == 1);
        this.locked.SetActive(song.isUnlocked == 0);
    }

}
