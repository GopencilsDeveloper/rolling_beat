﻿using UnityEngine;

public class Asteroid : MonoBehaviour
{
    private Transform rotater;
    private Transform obj;
    private Vector3 rand;
    private void Awake()
    {
        rotater = transform.GetChild(0);
        obj = rotater.GetChild(0);
        rand = new Vector3(Random.Range(0, 2), Random.Range(0, 2), Random.Range(0, 2)) * (Random.value < 0.5f ? 1 : -1);
    }
    public void Position(Pipe pipe, float curveRotation, float ringRotation)
    {
        int direction = Random.value < 0.5f ? 1 : -1;

        transform.SetParent(pipe.transform, false);
        transform.localRotation = Quaternion.Euler(0f, 0f, -curveRotation);
        rotater.localPosition = new Vector3(0f, pipe.CurveRadius + direction * (Random.Range(2, 4f)));
        rotater.localRotation = Quaternion.Euler(ringRotation, 0f, 0f);

        obj.rotation = Random.rotation;
    }
    private void Update()
    {
        obj.transform.position += rand * Time.deltaTime;
    }
}
